/*
 * Actuator discovery service polls, VirtualHub server on port 4444 for
 * relays, when a relay is found, the address is distinctivly added to the
 * remote database. The an actuator object is then added to the shared structure.
 * If an actuator is removed is the responsibility of this class to remove it 
 * from the structure.
 */
package actuatorController.actuatorProducer;

import actuatorController.database.IQueryManager;
import actuatorController.helpers.VirtualHubHelper;
import actuatorController.storage.Actuator;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;

/**
 *
 * @author babbleshack
 */
public class ActuatorDiscoveryService implements Runnable {
    private final ConcurrentMap<String, Actuator> _actuators;
    private final IQueryManager _qm;
    public ActuatorDiscoveryService(
            ConcurrentMap<String, Actuator> actuators, 
            IQueryManager queryManager) {
        _actuators = actuators;
        _qm = queryManager;
    }
    /**
     * gets a list of actuators, adds new ones to db and shared map.
     */
    @Override
    public void run() {
        while(true) {
            _actuators.putAll(VirtualHubHelper.getListOfActuators());
            _removeStaleRelays();
            for(String address : _actuators.keySet()){
                if(!_qm.isActuatorExists(address))
                    _qm.createActuatorRecord(address);
            }
        }
    }
    /**
     * removes stale relays from set
     * @param active relays 
     */
    private void _removeStaleRelays() {
        HashMap activeRelays = VirtualHubHelper.getListOfActuators();
        for(String address : _actuators.keySet()){
            if(activeRelays.containsKey(address))
                continue;
            _actuators.remove(address);
        }
    }
}

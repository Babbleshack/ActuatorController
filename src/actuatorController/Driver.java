package actuatorController;

import actuatorController.actuatorConsumer.ActuatorProcessor;
import actuatorController.actuatorProducer.ActuatorDiscoveryService;
import actuatorController.database.QueryManager;
import actuatorController.storage.Actuator;
import java.sql.SQLException;

import com.yoctopuce.YoctoAPI.YAPI;
import com.yoctopuce.YoctoAPI.YAPI_Exception;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Driver {
    private static final int THREAD_COUNT = 1;

    public static void main(String[] args) throws ClassNotFoundException, SQLException, YAPI_Exception {
        YAPI.RegisterHub("127.0.0.1");
        ExecutorService producer = Executors.newFixedThreadPool(THREAD_COUNT);
        ExecutorService consumer = Executors.newFixedThreadPool(THREAD_COUNT);
        ConcurrentMap<String, Actuator> actuatorMap =
                new ConcurrentHashMap<>();
        QueryManager qm = new QueryManager();
        producer.submit(new ActuatorDiscoveryService(actuatorMap, qm));
        consumer.submit(new ActuatorProcessor(actuatorMap, qm));
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actuatorController.actuatorConsumer;

import actuatorController.database.IQueryManager;
import actuatorController.helpers.VirtualHubHelper;
import actuatorController.storage.Actuator;
import java.util.concurrent.ConcurrentMap;
/**
 *
 * @author babbleshack
 */
public class ActuatorProcessor implements Runnable {
    private final ConcurrentMap<String, Actuator> _actuators;
    private final IQueryManager _qm;
    public ActuatorProcessor(
            ConcurrentMap<String, Actuator> actuators, 
            IQueryManager queryManager) {
        _actuators = actuators;
        _qm = queryManager;
    }
    /**
     * iterate actuators change status as set in database.
     */
    @Override
    public void run() {
        while(true){
            for(Actuator a: _actuators.values()){
                if(_qm.isActuatorNull(a.getActuatorAddress())) continue;
                if(_qm.isActuatorOn(a.getActuatorAddress()))
                    VirtualHubHelper.setRelayOn(a);
                else
                    VirtualHubHelper.setRelayOff(a);
            }
        }
    }
}

package actuatorController.database;


/**
 *
 * @author babbleshack
 */
public interface IQueryManager {

    /**
     * Insert spot record into db
     * @param actuator_address
     * @param time long
     */
    void createActuatorRecord(String actuator_address);


    /**
     * returns true if actuator address is found in database.
     * @param actuator_address
     * @return boolean
     */
    boolean isActuatorExists(String actuator_address);

    /**
     * returns true if actuator is null
     * @param actuator_address
     * @return
     */
    boolean isActuatorNull(String actuator_address);

    boolean isActuatorOn(String actuator_address);
    
}

/**
 *
 * @author Dominic Lindsay
 */
package actuatorController.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Connection;


public class QueryManager implements IQueryManager {
    
    private IDatabaseConnectionManager _connection;
    public QueryManager() {
       _connection = DatabaseConnectionFactory.createMySQLConnection();
    }
	/**
	 * Insert spot record into db
	 * @param actuator_address
	 * @param time long
	 */
        @Override
	public void createActuatorRecord(String actuator_address) {
        long time = System.currentTimeMillis();
        String insertActuatorRecord = "INSERT INTO Actuator"
                        + "(actuator_address, created_at, updated_at)"
                        + ("VALUES (?,?,?)");
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = _connection.getConnection();
            stmt = conn.prepareStatement(insertActuatorRecord);
            stmt.setString(1, actuator_address);
            stmt.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
            stmt.setTimestamp(3, new Timestamp(System.currentTimeMillis()));
            stmt.executeUpdate();
        } catch (Exception e){
            System.err.println("MySQL Query Error creating actuator record");
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            try { if (conn != null && conn.isClosed()) conn.close(); } catch (Exception e) {};
        }
	}

	/**
	 * returns true if actuator is null
	 * @param actuator_address
	 * @return
	 */
    @Override
	public boolean isActuatorNull(String actuator_address) {
        boolean result = false;
        String query = "SELECT * FROM Actuator WHERE actuator_address LIKE ?";
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = _connection.getConnection();
            stmt = conn.prepareStatement(query);
            stmt.setString(1, "%" + actuator_address.replace(".relay1", "") + "%");
            rs = stmt.executeQuery();
            if(rs.next()) {
                int returnedInt = rs.getInt("is_on");
                if(rs.wasNull())
                        result = true;
                else
                        result = false;
            } else {
                result = false;
            }
        } catch (Exception e){
            System.err.println("MySQL Query Error whilst checking Running status of:\t" + actuator_address);
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            try { if (conn != null && conn.isClosed()) conn.close(); } catch (Exception e) {};
        }
        return result;
	}

    @Override
	public boolean isActuatorOn(String actuator_address) {
        boolean result = false;
        String query = "SELECT * FROM Actuator WHERE actuator_address LIKE ?";
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = _connection.getConnection();
            stmt = conn.prepareStatement(query);
            stmt.setString(1, "%" + actuator_address.replace(".relay1", "") + "%");
            rs = stmt.executeQuery();
            if(rs.next()) {
                int returnedInt = rs.getInt("is_on");
                if(returnedInt > 0)
                        result = true;
                else
                        result = false;
            } else {
                result = false;
            }
        } catch (Exception e){
            System.err.println("MySQL Query Error whilst checking Null status of:\t" + actuator_address);
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            try { if (conn != null && conn.isClosed()) conn.close(); } catch (Exception e) {};
        }
        return result;
	}

	/**
	 *
	 * @param actuator_address
	 * @return
	 */
    @Override
	public boolean isActuatorExists(String actuator_address) {
        boolean result = false;        
        String query = "SELECT * FROM Actuator WHERE Actuator.actuator_address LIKE ? ";
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try {
            conn = _connection.getConnection();
            stmt = conn.prepareStatement(query);
            stmt.setString(1, "%" + actuator_address.replace(".relay1", "") + "%");
            rs = stmt.executeQuery();
            result = rs.next();
        } catch (Exception e){
            System.err.println("MySQL Query Error whilst checking Null status of:\t" + actuator_address);
            e.printStackTrace();
        } finally {
            try { if (rs != null) rs.close(); } catch (Exception e) {};
            try { if (stmt != null) stmt.close(); } catch (Exception e) {};
            try { if (conn != null && conn.isClosed()) conn.close(); } catch (Exception e) {};
        }
        return result;
	}
}

/*
 * helper class used to abstract virtual hub interface.
 */
package actuatorController.helpers;

import actuatorController.storage.Actuator;
import com.yoctopuce.YoctoAPI.YAPI;
import com.yoctopuce.YoctoAPI.YAPI_Exception;
import com.yoctopuce.YoctoAPI.YModule;
import com.yoctopuce.YoctoAPI.YRelay;
import java.util.HashMap;

/**
 *
 * @author babbleshack
 */
public class VirtualHubHelper {
    
    /**
     * polls the VirtualHub server and returns a Map of active actuators.
     * @return 
     */
    public static HashMap<String, Actuator> getListOfActuators(){
        try {
            YAPI.UpdateDeviceList();
        } catch (YAPI_Exception e) {
            System.err.println("Error occured whilst updating Device List.\n");
            e.printStackTrace();
        }
        YModule module = YModule.FirstModule(); 
        if(module == null) return null;
        HashMap<String, Actuator> actuators = new HashMap<>();
        Actuator act;
        while(module != null)
        {
            act = null;
            System.out.println("module: " + module);
            if(_checkIfRelayIsHub(module)) {
                module = module.nextModule();
                continue;
            }
            if((act = _createActuatorObject(module)) == null) continue;
            actuators.put(act.getActuatorAddress(), act);
            module = module.nextModule();
        }
            return actuators;
    }
    /**
     * determine if relay is Virtual Hub,
     * if fails assume its the hub
     * @param module
     * @return boolean
     */
    private static boolean _checkIfRelayIsHub(YModule module) {
        try {
            return module.get_productName().toLowerCase().contains("hub");
        } catch (YAPI_Exception ex) {
           System.err.println("Error getting product name\n");
           ex.printStackTrace();
        }
        return true;
    }
    /**
     * creates an Actuator Object obviously :p
     * @param module YModule
     * @return Actuator
     */
    private static Actuator _createActuatorObject(YModule module) {
        try {
            String relayAddress = module.getSerialNumber() + ".relay1"; //get relay1
            YRelay relay = YRelay.FindRelay(relayAddress);
            return new Actuator(relayAddress, relay);
        } catch (YAPI_Exception ex) {
            System.err.println("Error creating Actuator Obj\n");
            ex.printStackTrace();
        }
        return null;
    }
    /**
     * set relay on
     * @param actuator 
     */
    public static void setRelayOn(Actuator actuator){
        try {
                if(getRelayStatus(actuator)) return;
                if(!actuator.getRelay().isOnline()) return;
                actuator.getRelay().setState(1);
            } catch (YAPI_Exception e) {
                System.err.println("Can't turn relay ON");
                e.printStackTrace();
            }
    }
    /**
     * set relay off
     * @param actuator 
     */
    public static void setRelayOff(Actuator actuator){
        try {
                if(!getRelayStatus(actuator)) return;
                if(!actuator.getRelay().isOnline()) return;
                actuator.getRelay().setState(0);
            } catch (YAPI_Exception e) {
                System.err.println("Can't turn relay ON");
                e.printStackTrace();
            }
    }
    /**
     * returns true if relay is powered false otherwise
     * @param actuator
     * @return 
     */
    public static boolean getRelayStatus(Actuator actuator) {
        try {
            if(actuator.getRelay().getState() > 0) return true;
        } catch (YAPI_Exception ex) {
            System.err.println("Error getting Actuator state");
        }
        return false;
    }
    //getListOfActuators -> done.
    //SetRelayOn
    //SetRelayOff
}

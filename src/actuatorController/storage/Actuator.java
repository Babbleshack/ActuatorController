package actuatorController.storage;

import com.yoctopuce.YoctoAPI.*;

public class Actuator
{
	private YRelay _relay;
	private String _relayAddress;
	private boolean _isOn; // means status unknown yet
	private static final boolean ON = true;
	private static final boolean OFF = false;

	public Actuator(String name, YRelay relay1)
	{
		this._relay = relay1;
		this._relayAddress = name;
	}
        public YRelay getRelay() {
            return this._relay;
        }
        public void setRelay(YRelay relay) {
            this._relay = relay;
        }

	/**
	 * @return the actuatorAddress
	 */
	public String getActuatorAddress()
	{
		return _relayAddress;
	}

}